FROM node:12.18.3

WORKDIR /app

COPY node_modules ./node_modules
COPY build ./build

COPY server.js ./

EXPOSE 3000

CMD ["node", "server"]